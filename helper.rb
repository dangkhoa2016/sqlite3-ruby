require_relative './db_helper'

class Helper
  @db = DbHelper.new('database.sqlite3')
  TABLE_NAME = 'user'.freeze
  @mutex = Mutex.new

  def self.set_up(clear_data, seed)
    @db.execute <<END_SQL.gsub(/\s+/, ' ').strip
      CREATE TABLE IF NOT EXISTS user (email TEXT UNIQUE, username TEXT, point INTEGER);
END_SQL

    # clear data
    if clear_data
      puts "Delete all #{TABLE_NAME}"

      @db.execute("delete from #{TABLE_NAME} where rowid > ?", 0)
    end

    # seed
    if seed
      puts "Create 2 #{TABLE_NAME}"

      create_user({ email: 'email_1@test.local', username: 'user_1', point: 1 })
      create_user({ email: 'email_2@test.local', username: 'user_2', point: 2 })
    end
  end

  def self.email_exist?(email)
    return if email.nil?

    arr = @db.detail('email', TABLE_NAME, 'email = ?', email)
    present?(arr)
  end

  def self.create_user(user)
    return if user.nil? || user[:email].nil?

    puts "[#{Time.now}] Start create #{TABLE_NAME} #{user}"

    sleep(1)
    if email_exist?(user[:email])
      puts "Email #{user[:email]} already existed."
      return
    end

    # test sleep
    sleep(rand(0..3));

    u_id = @db.create(TABLE_NAME, '?, ?, ?', [user[:email], user[:username], user[:point]])
    puts "Created id #{u_id}"
  end

  def self.present?(arr)
    return if arr.nil?

    return arr.size > 0 if arr.respond_to?(:size)
    return !arr.empty? if arr.respond_to?(:empty?)

    !arr.blank? if arr.respond_to?(:blank?)
  end

  def self.create_user_with_lock(user)
    @mutex.synchronize do
      create_user(user)
    end
  end
end
