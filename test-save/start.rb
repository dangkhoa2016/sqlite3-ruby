# require_relative '../save/1-single.rb'

# require_relative '../save/2-multiple.1.rb'

# require_relative '../save/3-concurrency.1.rb'

require_relative '../save/4-lock.1'

=begin
arr = []
file_path = File.join(__dir__, "user.csv")
CSV.foreach(file_path, headers: true) { |row| arr << row }
=end

file_path = File.join(__dir__, "user.csv")
arr = CSV.read(file_path, headers: true, :header_converters => :symbol)

start_time = Time.new
puts "Start at: #{start_time}"

begin
  Test.new.start(arr)
rescue StandardError => e
  puts "error", e
end

end_time = Time.new
puts "End at: #{end_time}"

seconds_diff = (end_time - start_time).to_f.abs
puts "Total: #{seconds_diff} second(s)"
