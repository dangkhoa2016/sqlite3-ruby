require_relative '../helper.rb'

class Test
  def start(arr)
    return if arr.nil? || arr.size < 1

    threads = []
    
    arr.each do |item|
      threads << Thread.new(item) do
        Helper.create_user(item)
      end
    end

    threads.each { |t| t.join }
  end
end
