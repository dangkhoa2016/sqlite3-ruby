require_relative '../helper.rb'

class Test
  def start(arr)
    return if arr.nil? || arr.size < 1

    arr.each do |item|
      Helper.create_user(item)
    end
  end
end
