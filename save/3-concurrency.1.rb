require_relative '../helper.rb'
require './thread_pool.rb'

class Test
  MAX_ALLOW = 3

  def start(arr)
    return if arr.nil? || arr.size < 1

    pool = ThreadPool.new(size: MAX_ALLOW)

    arr.each do |item|
      pool.schedule { Helper.create_user(item) }
    end

    pool.run
  end
end
