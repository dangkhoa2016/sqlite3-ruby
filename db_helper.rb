
class DbHelper
  attr_accessor :db, :db_file

  def initialize(db_file)
    @db_file = db_file
    @db = SQLite3::Database.new @db_file
  end

  def create(table_with_columns, columns, values)
    execute("insert into #{table_with_columns} values (#{columns})", values)
    @db.last_insert_row_id
  end

  def detail(select_fields, table, where_conditions, values)
    execute("select #{select_fields} from #{table} where #{where_conditions}", values)
  end

  def delete(table, where_conditions, values)
    begin
      execute("delete from #{table} where #{where_conditions}", values)
      true
    rescue => e
      puts "Failed to delete", e
      false
    end
  end

  def update(table, set_conditions, where_conditions, values)
    begin
      execute("update #{table} set #{set_conditions} where #{where_conditions}", values)
      true
    rescue => e
      puts "Failed to update", e
      false
    end
  end

  def execute(sql, params = nil, &block)
    begin
      @db.execute(sql, params, &block)
    rescue => e
      puts "Failed to execute", e
    end
  end
end